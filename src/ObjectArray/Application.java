package ObjectArray;

public class Application {
	
	public static void main(String[] args) {
		
		
//		Car car1 = new Car("페라리", 300);
//		Car car2 = new Car("람보르기니", 350);
//		Car car3 = new Car("롤스로이스", 400);
//		Car car4 = new Car("부가티베이론", 540);
//		Car car5 = new Car("포터", 6450);
//		
//		car1.driveMaxSpeed();
//		car2.driveMaxSpeed();
//		car3.driveMaxSpeed();
//		car4.driveMaxSpeed();
//		car5.driveMaxSpeed();
		
		Car[] carArray = new Car[5];
		
		carArray[0] = new Car("페라리", 300);
		carArray[1] = new Car("람보르기니", 350);
		carArray[2] = new Car("롤스로이스", 400);
		carArray[3] = new Car("부가티베이론", 540);
		carArray[4] = new Car("포터", 6450);
		
		for(int i=0; i<carArray.length; i++) {
			carArray[i].driveMaxSpeed();
		}
		
		/* 객체 배열도 할당과 동시에 초기화 할수 있다. */
		
		Car[] carArray2 = {
				new Car("페라리", 300)   
				,new Car("람보르기니", 350)   
				,new Car("롤스로이스", 400)   
				,new Car("부가티베이론", 540)  
				,new Car("포터", 6450)     		
		};
		
		// 향상된 for문 사용 가능
		for(Car c : carArray2) {
			c.driveMaxSpeed();
		}
		
		
		
	}

}






















